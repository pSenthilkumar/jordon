server "192.241.143.99", :app, :web, :db, :primary => true
set :deploy_to, "/var/www/jlnew/"
set :branch, 'master'
set :scm_verbose, true
set :use_sudo, false
# set :rails_env, "development" #added for delayed job 
# set :rvm_type, :system
set :current_path, '/var/www/jlnew/wp-content/themes/jordonLondon'


# after 'deploy:update_code' do

# end

# namespace :deploy do
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end