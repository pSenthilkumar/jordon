<footer ng-app="footer">
  <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3 foot-border pad-Zero text-center">
    <span class="copyrights theme-text hidden-xs">&copy;2015 JL. Powered by <a target="_blank" class="theme-text" href="http://www.ninjaanimations.com/">NinjaAnimations.com.</a></span>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 pad-Zero">
    <div class="footLogo-holder text-center"></div>
    <div class="footLogo"><img src="<?php echo get_template_directory_uri();?>/images/footerLogo.png"></div>
  </div>
  <div class="col-lg-5 col-md-5 col-sm-4 col-xs-3 foot-border pad-Zero" ng-controller="SocialController">
    <ul class="list-unstyled social-icons hidden-xs">
      <li ng-repeat="social in sociallinks">
        <div ng-click="popitup(social.url)">
          <img ng-src="{{social.image}}" class="img-responsive" alt="">
        </div>
      </li>
    </ul>
  </div>
</footer>

<script type="text/javascript">var switchTo5x=true;</script>
<!-- <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script> -->
<!-- <script type="text/javascript">stLight.options({publisher: "741199c9-6162-4bea-9699-2d1a809d3d33", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script type="text/javascript">stLight.options({publisher: "7a9967af-3862-44b0-aa16-2ecc1c50cdf0", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
-->
</body>
</html>