<?php

function my_scripts() {

	// Libaries through Bower
	wp_register_script(
		'angularjs',
		get_template_directory_uri() . '/bower_components/angular/angular.min.js',
		array(),
		'0.0.1',
		true
	);

	wp_register_script(
		'angularjs-route',
		get_template_directory_uri() . '/bower_components/angular-route/angular-route.min.js',
		array(),
		'0.0.1',
		true
	);

	wp_register_script(
		'angularjs-resources',
		get_template_directory_uri() . '/bower_components/angular-resource/angular-resource.min.js',
		array(),
		'0.0.1',
		true
	);

	wp_register_script(
		'angularjs-sanitize',
		get_template_directory_uri() . '/bower_components/angular-sanitize/angular-sanitize.min.js',
		array(),
		'0.0.1',
		true
	);

	wp_register_script(
		'angularjs-media-player',
		get_template_directory_uri() . '/bower_components/angular-media-player/dist/angular-media-player.min.js',
		array(),
		'0.0.1',
		true
	);

	wp_register_script(
		'angular-youtube',
		get_template_directory_uri() . '/bower_components/angular-youtube-mb/dist/angular-youtube-embed.min.js',
		array(),
		'0.0.1',
		true
	);

	// Libaries through Bower

	wp_enqueue_script(
		'jquery',
		get_template_directory_uri() . '/js/jquery.js',
		array(),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'fancybox',
		get_template_directory_uri() . '/js/fancybox.js',
		array(),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'magnific_popup',
		get_template_directory_uri() . '/js/jquery.magnific-popup.min.js',
		array(),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'gallery',
		get_template_directory_uri() . '/js/gallery.js',
		array(),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'slick',
		get_template_directory_uri() . '/js/slick.min.js',
		array(),
		'0.0.1',
		true
	);
	// App js
	wp_enqueue_script(
		'app',
		get_template_directory_uri() . '/js/angularjs/app.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	// App js

	// Factories
	wp_enqueue_script(
		'postFactory',
		get_template_directory_uri() . '/js/angularjs/factory/postFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'musicFactory',
		get_template_directory_uri() . '/js/angularjs/factory/musicFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'socialFactory',
		get_template_directory_uri() . '/js/angularjs/factory/socialFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'videoFactory',
		get_template_directory_uri() . '/js/angularjs/factory/videoFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player', 'angular-youtube'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'galleryFactory',
		get_template_directory_uri() . '/js/angularjs/factory/galleryFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player', 'angular-youtube'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'userFactory',
		get_template_directory_uri() . '/js/angularjs/factory/userFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player', 'angular-youtube'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'blogFactory',
		get_template_directory_uri() . '/js/angularjs/factory/blogFactory.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player', 'angular-youtube'),
		'0.0.1',
		true
	);

	// Factories

	// shared
	wp_enqueue_script(
		'directives',
		get_template_directory_uri() . '/js/angularjs/shared/directives.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'routes',
		get_template_directory_uri() . '/js/angularjs/shared/routes.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'resources',
		get_template_directory_uri() . '/js/angularjs/shared/resources.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	// shared

	// controllers
	wp_enqueue_script(
		'musicController',
		get_template_directory_uri() . '/js/angularjs/controllers/musicController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'SocialController',
		get_template_directory_uri() . '/js/angularjs/controllers/socialController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);

	wp_enqueue_script(
		'videoController',
		get_template_directory_uri() . '/js/angularjs/controllers/videoController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'galleryController',
		get_template_directory_uri() . '/js/angularjs/controllers/galleryController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'blogController',
		get_template_directory_uri() . '/js/angularjs/controllers/blogController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	wp_enqueue_script(
		'biographyController',
		get_template_directory_uri() . '/js/angularjs/controllers/biographyController.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources', 'angularjs-media-player'),
		'0.0.1',
		true
	);
	// wp_enqueue_script(
	// 	'buttonjs',
	// 	get_template_directory_uri() . '/js/buttons.js',
	// 	array(),
	// 	'0.0.1',
	// 	true
	// );
	wp_enqueue_script(
		'biography',
		get_template_directory_uri() . '/js/angularjs/factory/biography.js',
		array('angularjs', 'angularjs-route', 'angularjs-sanitize', 'angularjs-resources'),
		'0.0.1',
		true
	);
	// controllers

	// Loading Partials
	wp_localize_script(
		'app',
		'myLocalized',
		array(
			'partials' => trailingslashit(get_template_directory_uri()) . 'partials/',
		),
		'0.0.1',
		true
	);
	// Loading Partials

	wp_enqueue_script(
		'bootstrap',
		get_template_directory_uri() . '/js/bootstrap.min.js',
		array(),
		'0.0.1',
		true
	);

}

add_action('wp_enqueue_scripts', 'my_scripts');

add_action('wp_ajax_update_views', 'update_views');
add_action('wp_ajax_nopriv_update_views', 'update_views');

function update_views() {
	// var_dump($_GET);
	session_start();
	$response = array();
	$response[0] = 'success';
	if (!$_SESSION["post"]) {
		$_SESSION["post"] = array();
	}
	echo "sfhsfkjsjdf";
	array_push($_SESSION['post'], $_GET["id"]);
	$field_key = 'viewcount';
	$post_id = $_GET["id"];
	$value = get_field($field_key, $post_id) + 1;
	update_field($field_key, $value, $post_id);
	$response[0] = 'success';
	print_r($response);
	die();
}

add_action('wp_ajax_update_stream', 'update_stream');
add_action('wp_ajax_nopriv_update_stream', 'update_stream');

function update_stream() {
	$field_key = 'streamcount';
	$post_id = $_GET["id"];
	$index = $_GET["index"];
	$row = get_field('songs', $post_id);
	$value = intval($row[$index]['streamcount']) + 1;
	update_sub_field(array('songs', $index + 1, $field_key), $value, $post_id);
	die();
}

add_action('wp_ajax_update_download', 'update_download');
add_action('wp_ajax_nopriv_update_download', 'update_download');

function update_download() {
	session_start();
	$response = array();
	$response[0] = 'success';
	if (!$_SESSION["post"]) {
		$_SESSION["post"] = array();
	}
	var_dump("ssagfkjfkgslfdg");
	array_push($_SESSION['post'], $_GET["id"]);
	$field_key = 'downloadcount';
	$post_id = $_GET["id"];
	$value = get_field($field_key, $post_id) + 1;
	update_field($field_key, $value, $post_id);
	$response[0] = 'success';
	die();
}

add_action('wp_ajax_top_songs', 'top_songs');
add_action('wp_ajax_nopriv_top_songs', 'top_songs');

function top_songs() {
	$args = array('posts_per_page' => 10, 'offset' => 0, 'category_name' => 'playlist');
	$myposts = get_posts($args);
	$allSongs = array();
	foreach ($myposts as $post) {
		$songs = get_field('songs', $post->ID);
		for ($i = 0; $i < count($songs); $i++) {
			$songs[$i]['author'] = $post->ID;
		}
		$allSongs = array_merge($allSongs, $songs);
	}

	usort($allSongs, "cmp");

	echo json_encode($allSongs);
	die();
}

add_action('wp_ajax_get_zip', 'get_zip');
add_action('wp_ajax_nopriv_get_zip', 'get_zip');

function get_zip() {
	$post_id = $_GET["id"];
	$zipfile = get_field('zipfile', $post_id);
	if ($zipfile != 1) {
		echo $zipfile;
	} else {
		$songs = get_field('songs', $post_id);
		foreach ($songs as $song) {

		}
	}
	die();
}

add_action('wp_ajax_is_user_authenticated', 'is_user_authenticated');
add_action('wp_ajax_nopriv_user_authenticated', 'is_user_authenticated');
function is_user_authenticated() {
	if (is_user_logged_in()) {
		$currentuser = wp_get_current_user();
		echo json_encode($currentuser);
	} else {
		return false;
	}
	die();
}

add_action('wp_ajax_create_user', 'create_user_from_ajax');
add_action('wp_ajax_nopriv_create_user', 'create_user_from_ajax');

function create_user_from_ajax() {
	$user_id = null;
	$user_email = sanitize_text_field($_POST['user_email']);
	if (null == username_exists($user_email)) {
		$user_name = sanitize_text_field($_POST['user_name']);
		$user_password = sanitize_text_field($_POST['password']);
		$user_id = wp_create_user($user_name, $password, $user_email);
		wp_signon(array("user_login" => $user_email, "user_password" => $user_password), false);
	}
	echo json_encode(array("user_id" => $user_id));
	die();
}
add_action('wp_ajax_get_all_users', 'get_all_users');
add_action('wp_ajax_nopriv_get_all_users', 'get_all_users');
function get_all_users() {
	$userdata = get_users();
	echo json_encode($userdata);
	die();
}

add_action('wp_ajax_login_user', 'login_user');
add_action('wp_ajax_nopriv_login_user', 'login_user');

function login_user() {
	$user_id = 0;

	$username = sanitize_text_field($_POST['user_email']);
	$password = sanitize_text_field($_POST['password']);

	if (username_exists($username)) {

		$creds = array(
			'user_login' => $username,
			'user_password' => $password,
		);
		$user = wp_signon($creds, false);
		$status = is_wp_error($user) ? 0 : 1;
		$user_id = $user->id;
	}
	echo json_encode(array("user_id" => $user_id));
	die();
}
add_action('wp_ajax_forgot_password', 'forgot_password');
add_action('wp_ajax_nopriv_forgot_password', 'forgot_password');

function forgot_password() {
	global $wpdb, $wp_hasher;

	$user_login = sanitize_text_field($_POST['user_email']);

	if (empty($user_login)) {
		return false;
	} else if (strpos($user_login, '@')) {
		$user_data = get_user_by('email', trim($user_login));
		if (empty($user_data)) {
			return false;
		}

	} else {
		$login = trim($user_login);
		$user_data = get_user_by('login', $login);
	}

	do_action('lostpassword_post');

	if (!$user_data) {
		return false;
	}

	// redefining user_login ensures we return the right case in the email
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;

	do_action('retreive_password', $user_login); // Misspelled and deprecated
	do_action('retrieve_password', $user_login);

	$allow = apply_filters('allow_password_reset', true, $user_data->ID);

	if (!$allow) {
		return false;
	} else if (is_wp_error($allow)) {
		return false;
	}

	$key = wp_generate_password(20, false);
	do_action('retrieve_password_key', $user_login, $key);

	if (empty($wp_hasher)) {
		require_once ABSPATH . 'wp-includes/class-phpass.php';
		$wp_hasher = new PasswordHash(8, true);
	}
	$hashed = $wp_hasher->HashPassword($key);
	$wpdb->update($wpdb->users, array('user_activation_key' => $hashed), array('user_login' => $user_login));

	$message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
	$message .= network_home_url('/') . "\r\n\r\n";
	$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
	$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
	$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
	$message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

	if (is_multisite()) {
		$blogname = $GLOBALS['current_site']->site_name;
	} else {
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
	}

	$title = sprintf(__('[%s] Password Reset'), $blogname);

	$title = apply_filters('retrieve_password_title', $title);
	$message = apply_filters('retrieve_password_message', $message, $key);

	if ($message && !mail($user_email, $title, $message)) {
		wp_die(__('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...'));
	}

	echo 'Link for password reset has been emailed to you. Please check your email.';
	die();
}

add_action('wp_ajax_recent_blogs', 'recent_blogs');
add_action('wp_ajax_nopriv_recent_blogs', 'recent_blogs');

function recent_blogs() {
	$args = array('category_name' => 'blog', 'posts_per_page' => 3, 'orderby' => 'ID', 'order' => 'DESC');
	$myposts = get_posts($args);
	$result = blog_post_info($myposts);
	echo json_encode($result);
	die();
}

add_action('wp_ajax_popular_blogs', 'popular_blogs');
add_action('wp_ajax_nopriv_popular_blogs', 'popular_blogs');

function popular_blogs() {
	$page = $_GET['page'];
	$args = array('category_name' => 'blog', 'posts_per_page' => 4, 'orderby' => 'id', 'order' => 'DESC', 'paged' => $page);
	$myposts = get_posts($args);
	$result = blog_post_info($myposts);
	echo json_encode($result);
	die();
}

//Mail function
add_action('wp_ajax_contact_mail', 'contact_mail');
add_action('wp_ajax_nopriv_contact_mail', 'contact_mail');

function contact_mail() {
	$first = $_POST["firstname"];
	$last = $_POST["lastname"];
	$email = $_POST["email"];
	$address = $_POST["address"];
	$message = $_POST["message"];
	$to = "thamizh2k7@gmail.com";
	wp_mail($to, $first, $last, $email, $address, $message);
	die("Hello mail sent");
}

add_action('wp_ajax_get_blog_comments', 'get_blog_comments');
add_action('wp_ajax_nopriv_get_blog_comments', 'get_blog_comments');

function get_blog_comments() {
	$post_id = $_GET['post_id'];
	$args = array('post_id' => $post_id);
	$comments = get_comments($args);

	echo json_encode($comments);
	die();
}

add_action('wp_ajax_save_comment', 'save_comment');
add_action('wp_ajax_nopriv_save_comment', 'save_comment');

function save_comment() {
	$time = current_time('mysql');

	$user = get_userdata(sanitize_text_field($_POST['user_id']));

	$data = array(
		'comment_post_ID' => sanitize_text_field($_POST['post_id']),
		'comment_content' => sanitize_text_field($_POST['comment']),
		'user_id' => $user->id,
		'comment_date' => $time,
		'comment_approved' => 1,
	);
	wp_insert_comment($data);
	die();
}

add_action('wp_ajax_related_blogs', 'related_blogs');
add_action('wp_ajax_nopriv_related_blogs', 'related_blogs');

function related_blogs() {
	$post_id = $_GET['id'];
	global $post;
	$tags = wp_get_post_tags($post_id);
	if ($tags) {
		$tag_ids = array();
		foreach ($tags as $individual_tag) {
			$tag_ids[] = $individual_tag->term_id;
		}

		$args = array(
			'tag__in' => $tag_ids,
			'post__not_in' => array($post->ID),
			'posts_per_page' => 4, // Number of related posts to display.
			'caller_get_posts' => 1,
		);
		$myposts = get_posts($args);
		echo json_encode($myposts);
	}

	die();
}
add_action('wp_ajax_save_rate', 'save_rate');
add_action('wp_ajax_nopriv_save_rate', 'save_rate');

function save_rate() {
	$time = current_time('mysql');
	global $wpdb;
	$table_name = $wpdb->prefix . "postrate";
	$wpdb->insert($table_name, array('post' => $_POST['post_id'], 'rate' => $_POST['rate'], 'user' => $_POST['user_id']));
// 	$user = get_userdata(sanitize_text_field( $_POST['user_id'] ));

// 	$data = array(
	// 	   'comment_post_ID' => sanitize_text_field( $_POST['post_id'] ),
	// 	    'comment_content' => sanitize_text_field( $_POST['comment'] ),
	// 	    'user_id' => $user->id,
	// 	    'comment_date' => $time,
	// 	    'comment_approved' => 1,
	// 	);

// wp_insert_comment($data);

// 	die();
}
add_action('wp_ajax_get_rating', 'get_rating');
add_action('wp_ajax_nopriv_get_rating', 'get_rating');

function get_rating() {
	global $wpdb;
	$table = $wpdb->prefix . "postrate";
	$data = $wpdb->get_results("SELECT * FROM $table");
	echo json_encode($data);
	die();
}

function cmp($a, $b) {
	return intval($b['streamcount']) - intval($a['streamcount']);
}

function blog_post_info($posts = array()) {
	$blog_result = array();
	foreach ($posts as $post) {
		$blog_post = array();
		$blog_image = get_field('blog_image', $post->ID);
		$blog_description = get_field('description', $post->ID);
		$video_url = get_field('video_url', $post->ID);
		$blog_post['blog_image'] = $blog_image;
		$blog_post['description'] = $blog_description;
		$blog_post['video_url'] = $video_url;
		$blog_post['author'] = get_userdata($post->post_author)->user_login;
		$blog_post['date'] = $post->post_date;
		$blog_post['blog_id'] = $post->ID;
		$blog_post['title'] = $post->post_title;
		array_push($blog_result, $blog_post);
	}
	return $blog_result;
}
