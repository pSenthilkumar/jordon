<!DOCTYPE html>
<html>
<head>
  <title>Jordon London</title>
  <?php wp_head();?>


  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="google-site-verification" content="" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <?php
if ($_SERVER[HTTP_HOST] == 'localhost') {
	$url = split("/", $_SERVER[REQUEST_URI]);
	$url = "/" . $url[1] . "/";
} else {
	$url = "http://" . $_SERVER[HTTP_HOST] . "/";
}
?>
  <base href="<?php echo $url;?>">

  <!-- Favicons -->
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/logo.png">

  <!-- CSS -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css"><!-- / bootstrap styles -->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>"><!-- / template styles -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/fonts.css"><!-- / template styles -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/font-awesome.css"><!-- / template styles -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/fancybox.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick-theme.css">
</head>
<body  ng-app="app"
       ng-class="(path == '/biography' || path == '/gallery')?'biographyBg':(path == '/')?'homeBg':'pageBg'">
  <div style="display:none" id="security_field"><?php wp_nonce_field('ajax-login-nonce', 'security');?></div>
  <header>
    <nav class="navbar header">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed headerToggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse headerCollapse">
          <ul class="nav navbar-nav">
            <li><a class="Top-navigation" ng-class="(currentPath == '/')? 'active':''" href="<?php echo $url;?>">Home</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/music')? 'active':''" href="music">Music</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/videos')? 'active':''" href="videos">Videos</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/gallery')? 'active':''" href="gallery">Gallery</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/biography')? 'active':''" href="biography">Biography</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/contact')? 'active':''" href="contact">Contact</a></li>
            <li><a class="Top-navigation" ng-class="(currentPath == '/blog')? 'active':''" href="blog">blog</a></li>
            <li>
              <a class="Top-navigation" ng-class="(currentPath == '/freemixtape')? 'active':''" href="freemixtape">
                MixTape
              </a>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="border-down"></div>
  </header>
  <?php
$auth = wp_authenticate_username_password("admin", "admin", "admin");

if (is_wp_error($auth)) {

	$check = "user is accept";

} else {

	$check = "user is not accept";

}
?>
<script type="text/javascript">
  var ajaxurl = "<?php echo admin_url('admin-ajax.php');?>";
  var checker= "<?php echo $check;?>";
  var api_url = "<?php echo 'wp-login.php'?>";
  console.log(checker);

  jQuery(".Top-navigation").click(function(event) {
    jQuery(".Top-navigation.active").removeClass('active');
    jQuery(this).addClass('active');
  });

  jQuery(".navbar-collapse li a").click(function(event) {
    jQuery(".navbar-collapse").removeClass('in');
  });

  var isOpen=false;
  jQuery ("body").on("click",function(){
    if(!isOpen){
      jQuery(".navbar-collapse").removeClass('in');
      isOpen=true;
    }
  });
</script>