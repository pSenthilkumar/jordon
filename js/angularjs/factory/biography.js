app.factory('BiographyFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
	var biographyFactory={};
	biographyFactory.model = {
		"biography": []
	};
	biographyFactory.biography = function(){
	    return Post.biography().$promise.then(function(data){
	      biographyFactory.model.biography = data
	      console.log (data);
	    });
	}  	
  	return biographyFactory;
}])