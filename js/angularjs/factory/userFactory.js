app.factory('UserFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "User", function($resource,$http,$location,$rootScope,$q,$timeout,User){
	var UserFactory = {};
	UserFactory.model = {
		"user_id" : undefined
	}
	
  UserFactory.createUser = function(user_data){
    return User.register(null,user_data).$promise
  }
  UserFactory.loginUser = function(user_data){
    return User.login(null,user_data).$promise
  }
  UserFactory.send_mail = function(user_data){
     return User.contact(null,user_data).$promise
  }
  UserFactory.forgotPassword = function(user_data){
     return User.forgot(null,user_data).$promise
  }
  return UserFactory;
}])