app.factory('SocialFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
	var socialFactory = {};
	socialFactory.model = {
		"links" : []
	}
	
  socialFactory.socialLinks = function(){
    return Post.socialLinks().$promise.then(function(data){
      socialFactory.model.links = data
      console.log(data);
    });
  }
  return socialFactory;
}])