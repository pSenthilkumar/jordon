app.factory('BlogFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
	var BlogFactory = {};
	BlogFactory.model = {
		"recent_blogs" : [],
    "popular_blogs" : [],
    "related_blogs" : []
	}

  BlogFactory.recentBlogs = function(){
    return Post.recentBlogs().$promise.then(function(data){
      BlogFactory.model.recent_blogs = data
    });
  }

  BlogFactory.popularBlogs = function(){
    return Post.popularBlogs().$promise.then(function(data){
      console.log(data);
      BlogFactory.model.popular_blogs = data
    });
  }
  BlogFactory.relatedBlogs = function(){
    return Post.relatedBlogs().$promise.then(function(data){
      BlogFactory.model.related_blogs = data
    });
  }
  return BlogFactory;
}])