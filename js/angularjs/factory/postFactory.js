app.factory('Postfactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
  var Postfactory = {}
  
  Postfactory.model = {
    "posts": undefined,
    "currentPost": undefined,
    "playlists": undefined,
    "comments" : [],
    "user_status" : undefined
  } 

  Postfactory.getPost = function(id){
    return Post.get({ id: id }).$promise.then(function(data){
      Postfactory.model.currentPost = data
    });
  }

  Postfactory.getPosts = function(){
    return Post.getposts().$promise.then(function(data){
      Postfactory.model.playlists = data
    });
  }

  Postfactory.getUserStatus = function(){
    return Post.userDetail().$promise.then(function(data){
      Postfactory.model.user_status = data
    })
  } 
  Postfactory.getPostComments = function(post_id){
    return Post.getComments({"post_id":post_id}).$promise.then(function(data){
      Postfactory.model.comments = data
    })
  }  

  return Postfactory;
}])
