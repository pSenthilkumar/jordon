app.factory('VideoFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
	var videoFactory = {};
	videoFactory.model = {
		"featured_videos":[],
		"playingNow":undefined,
		"popular_videos":[]
	}
	videoFactory.setPlayingNow = function(video){
		videoFactory.model.playingNow = video
	}
	videoFactory.setPopularVideos = function(videos){
		videoFactory.model.popular_videos=videos
	}
	videoFactory.setFeaturedVideos = function(videos){
		videoFactory.model.featured_videos = videos
	}
	videoFactory.updateViewCount = function(post){
		Post.updateViewCount(post.id,index)
	}
	videoFactory.getFeaturedVideos = function(){
    return Post.featuredVideos().$promise.then(function(data){
      videoFactory.model.featured_videos = data
    });
  }
  videoFactory.getPopularVideos = function(){
    return Post.popularVideos().$promise.then(function(data){
      videoFactory.model.popular_videos = data
    });
  }
  return videoFactory;

}])