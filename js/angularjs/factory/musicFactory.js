app.factory('MusicFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
  var musicFactory = {}
  var user=[]  
  musicFactory.rateResult=[]
  musicFactory.model = {
    "currentPlaylist": undefined,
    "nowPlaying": undefined,
    "topSongs": undefined,
    "songs": [],
    "comment":[]
  } 

  musicFactory.setPlaying = function(playlist){
    musicFactory.model.currentPlaylist = playlist
    musicFactory.model.nowPlaying = playlist.meta.songs.map(function (song, index, array) {
      var parseTitle = song.mp3.title
      return { src: song.mp3.url, type: 'audio/mp3', title: parseTitle };
    });
  }
  musicFactory.getComment=function(postId) {
    console.log(postId);
    Post.getAllUserName().$promise.then(function(commentData){
      user=commentData;
    }).then(function(){
      Post.getComments().$promise.then(function(data){
        var i=0;
        var j=0;
        musicFactory.model.comment=[];
        while(i< data.length){
          if (data[i].comment_post_ID==postId) {
            
            while(j < user.length){
              if (user[j].ID==data[i].user_id) {
                var commentArray={"username":user[j].data.user_login,"comment":data[i].comment_content}
                musicFactory.model.comment.push(commentArray);
              }
              j+=1;
            }            
            j=0;
          }          
          i+=1;
        }   
        i=0;     
      });
    })

  }
  musicFactory.addSongs = function(song){
    musicFactory.model.songs.push(song)
  }

  musicFactory.addListen = function(post){    
    console.log(post);
    Post.updateViews({id:post})
  }

  musicFactory.addStream = function(songId,index){
    Post.updateStream({id:songId,index:index})
  }
  musicFactory.addDownload = function(post){
    Post.updateDownload({id:post})
  }

  musicFactory.setTopSongs = function(songs){
    musicFactory.model.topSongs = songs;
  }

  musicFactory.topSongs = function(){
    return Post.topSongs();
  }

  musicFactory.download = function(playlistID){
    return Post.download({id:playlistID});
  }
  musicFactory.getRating = function(){
    return Post.getRate().$promise.then(function(data){
      musicFactory.rateResult=data
    });
  }
  return musicFactory;
}])
