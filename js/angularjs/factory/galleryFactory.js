app.factory('GalleryFactory', ["$resource","$http", "$location", "$rootScope", "$q", "$timeout", "Post", function($resource,$http,$location,$rootScope,$q,$timeout,Post){
	var galleryFactory = {};
	galleryFactory.model = {
		"gallery_images" : []
	}
	
  galleryFactory.galleryImages = function(){
    return Post.galleryImages().$promise.then(function(data){
      galleryFactory.model.gallery_images = data
    });
  }
  return galleryFactory;
}])