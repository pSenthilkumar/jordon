//Config the route
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $routeProvider
  .when('/', {
    templateUrl: myLocalized.partials + 'main.html',
    controller: 'videoController'
  })
  .when('/music', {
    templateUrl: myLocalized.partials + 'music.html',
    controller: 'musicController'
  })
  .when('/videos', {
    templateUrl: myLocalized.partials + 'videos.html',
    controller: 'videoController'
  })
  .when('/videos/:video_id', {
    templateUrl: myLocalized.partials + 'videos.html',
    controller: 'videoController'
  })
  .when('/gallery', {
    templateUrl: myLocalized.partials + 'gallery.html',
    controller: 'galleryController'
  })
  .when('/biography', {
    templateUrl: myLocalized.partials + 'biography.html',
    controller: 'biographyController'
  })
  .when('/contact', {
    templateUrl: myLocalized.partials + 'contact.html',
    controller: 'musicController'
  })
  .when('/freemixtape', {
    templateUrl: myLocalized.partials + 'freemixtape.html',
    controller: 'musicController'
  })
  .when('/freemixtape/:album', {
    templateUrl: myLocalized.partials + 'freemixtape.html',
    controller: 'musicController'
  })
  .when('/blog', {
    templateUrl: myLocalized.partials + 'blog.html',
    controller: 'blogController'
  })
  .when('/blogs/:blog_id', {
    templateUrl: myLocalized.partials + 'blogs.html',
    controller: 'blogController'
  }) 
  .when('/functions.php', {
    templateUrl: 'http://localhost/wordpress/functions.php',
    controller: 'musicController'
  }) 
  .otherwise({
    redirectTo: '/'
  });
}]).run(
 ["$rootScope", "$location", "$route", "$timeout", function($rootScope, $location, $route, $timeout) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      $rootScope.path = $location.path();
    });
 }]);