//searchForm Directive
app.directive('searchForm', function() {
  return {
    restrict: 'EA',
    template: 'Search Keyword: <input type="text" name="s" ng-model="filter.s" ng-change="search()">',
    controller: ['$scope', '$http', function ( $scope, $http ) {
      $scope.filter = {
        s: ''
      };
      $scope.search = function() {
        $http.get('wp-json/posts/?filter[s]=' + $scope.filter.s).success(function(res){
          $scope.posts = res;
        });
      };
    }]
  };
});

app.directive('magnific',function() {
  return {
    restrict: 'C',
    link: function($scope, element, attr) {
      element.magnificPopup({
        delegate: 'a', 
        type: 'image',
        gallery: {
          enabled: true,          
        }
      });
    }
  }
});


app.directive('fancyBoxx', function(){
  return{
    restrict: 'A',
    link: function(scope,element,attrs){
      element.fancybox();
    }
  };
});

app.directive('mypopover', function ($compile,$templateCache) {

  var getTemplate = function (contentType) {
    var template = '';
    switch (contentType) {
      case 'user':
      template = $templateCache.get("templateId.html");
      break;
    }
    return template;
  }
  return {
    restrict: "C",
    link: function (scope, element, attrs) {
      var popOverContent;
      
      popOverContent = getTemplate("user");     

      popOverContent = $compile("<div>" + popOverContent+"</div>")(scope);

      var options = {
        content: popOverContent,
        placement: "bottom",
        html: true,
        date: scope.date
      };
      jQuery(element).popover(options);
    }
  };
});
app.directive('toggleText',function(){
  return{
    restrict: 'A',
    link: function(scope,element,attrs){
      scope.toggle=function(){
        setTimeout(function(){
          element.html("Send Message");
        },2500);
      };
    }
  };
});
app.directive('toggleComment',function(){
  return{
    restrict: 'A',
    link: function(scope,element,attrs){
      scope.changeComment=function(){
        setTimeout(function(){
          element.html("Add Comment");
        },2500);
      };
    }
  };
});