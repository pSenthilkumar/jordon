app.factory('Post', function($resource) {
  return $resource('wp-json/posts/:id',{
    id: '@id'
  },{
    'getposts': { 
      method:'GET',
      url:'wp-json/posts?filter[category_name]=playlist',
      isArray: true
    },
    'updateViews': {
      method:'GET',
      params: {action:'update_views'},
      url: ajaxurl,
      isArray: false
    },
    'updateStream': {
      method:'GET',
      params: {action:'update_stream'},
      url: ajaxurl,
      isArray: true
    },
    'updateDownload': {
      method:'GET',
      params: {action:'update_download'},
      url: ajaxurl,
      isArray: true
    },
    'topSongs': {
      method:'GET',
      params: {action:'top_songs'},
      url: ajaxurl,
      isArray: true
    },
    'download': {
      method:'GET',
      params: {action:'get_zip'},
      url: ajaxurl,
      isArray: true
    },
    'userDetail':{
      method:'GET',
      params: {action:'is_user_authenticated'},
      url: ajaxurl,
      isArray: false
    },
    'popularVideos':{
      method:'GET',
      url:'wp-json/posts?filter[category_name]=videos',
      isArray: true
    },
    'socialLinks':{
      method:'GET',
      url:'wp-json/posts?filter[category_name]=sociallinks',
      isArray: true
    },
    'featuredVideos':{
      method:'GET',
      url:'wp-json/posts?filter[category_name]=videos',
      isArray: true
    },
    'galleryImages':{
      method:'GET',
      url:'wp-json/posts?filter[category_name]=gallery',
      isArray: true
    },
    'recentBlogs':{
      url:ajaxurl,
      method:'GET',
      params:{action:'recent_blogs'},
      isArray: true
    },
    'getAllUserName':{
      method:'GET',
      params: {action:'get_all_users'},
      url: ajaxurl,
      isArray: true
    },
    'popularBlogs':{
      url:ajaxurl,
      method:'GET',
      params:{action:'popular_blogs'},
      isArray: true
    },
    'getComments':{
      method:'GET',
      url:ajaxurl,
      params:{action:"get_blog_comments"},
      isArray: true
    },    
    'biography':{
      method:'GET',
      url:'wp-json/posts?filter[category_name]=biography',
      isArray: true
    },
    'getRate':{
      method:'GET',
      params: {action:'get_rating'},
      url: ajaxurl,
      isArray: true
    }
  });
});

app.factory('Comment',function($resource){
  return $resource(ajaxurl,null,{
    'postComment':{
      method:"POST",
      isArray:false,
      params:{"action":"save_comment"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    }
  });
});
app.factory('Rate',function($resource){
  return $resource(ajaxurl,null,{
    'postRate':{
      method:"POST",
      isArray:false,
      params:{"action":"save_rate"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    }
  });

});

app.factory('User',function($resource){
  return $resource(ajaxurl,null,{

    'register':{
      method:"POST",
      isArray:false,
      params:{"action":"create_user"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    },
    'login':{
      method:"POST",
      isArray:false,
      params:{"action":"login_user"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    },
    'forgot':{
      method:"POST",
      isArray:false,
      params:{"action":"forgot_password"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    },
    'contact':{
      method:"POST",
      isArray:false,
      params:{"action":"contact_mail"},
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      }
    }    

  });
});


// localhost/wordpress3/wp-json/posts?filter[category_name]=playlist