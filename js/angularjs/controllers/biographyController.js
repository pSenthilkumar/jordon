app.controller('biographyController', ['$rootScope','$routeParams', '$scope','$location','$route','$http', 'Postfactory','BiographyFactory','SocialFactory', function($rootScope, $routeParams, $scope, $location, $route, $http,Postfactory,BiographyFactory,SocialFactory) {
  $scope.biography = [];
  var biography  = BiographyFactory.biography().then(function(){
  	$scope.biography  = BiographyFactory.model.biography[0];
    console.log($scope.biography.content);
  });
  $scope.sociallinks=[];
  var sociallinks = SocialFactory.socialLinks().then(function(){
    $scope.sociallinks=SocialFactory.model.links[0].meta.links
    console.log(SocialFactory.model.links[0].meta.links);
  });
  $scope.popitup=function(url) {
    var left = screen.width;
    var top = (screen.height/2)-(225);
    console.log(left);
    console.log(top);
    return window.open(url,"facebook",'height=300,width=500, top=' + top + ', left=' + left);  
  }
}])