app.controller('galleryController', ['$rootScope','$routeParams', '$scope','$location','$route','$http', 'Postfactory','GalleryFactory','SocialFactory', function($rootScope, $routeParams, $scope, $location, $route, $http,Postfactory,GalleryFactory,SocialFactory) {
  $scope.gallery_images = [];
  var gallery_images = GalleryFactory.galleryImages().then(function(){
  	$scope.gallery_images = GalleryFactory.model.gallery_images[0].meta.gallery
    $scope.gallery_top_images=$scope.gallery_images.slice(0,2)
    var gallery_length=$scope.gallery_images.length
    $scope.gallery_images=$scope.gallery_images.slice(2,gallery_length)
    console.log($scope.gallery_images);
  });
  $scope.sociallinks=[];
  var sociallinks = SocialFactory.socialLinks().then(function(){
  	$scope.sociallinks=SocialFactory.model.links[0].meta.links
  	console.log(SocialFactory.model.links[0].meta.links);
  });
  $scope.popitup=function(url) {
    var left = screen.width;
    var top = (screen.height/2)-(225);
    console.log(left);
    console.log(top);
    return window.open(url,"facebook",'height=300,width=500, top=' + top + ', left=' + left);  
  }
}])