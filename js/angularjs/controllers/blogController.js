app.controller('blogController', ['$rootScope','$routeParams', '$scope','$location','$route','$http', 'Postfactory','BlogFactory','SocialFactory', function($rootScope, $routeParams, $scope, $location, $route, $http, Postfactory,BlogFactory,SocialFactory) {
  $rootScope.currentPath = $route.current.templateUrl.split("/").pop(-1);
  console.log($rootScope.currentPath);
  $scope.blog_video=0
  $scope.recent_blogs=[]
  $scope.popular_blogs=[]
  
  BlogFactory.recentBlogs().then(function(){
    $scope.recent_blogs = BlogFactory.model.recent_blogs
    console.log($scope.recent_blogs);
    var i=0;
    while(i<$scope.recent_blogs.length){
      var regex = new RegExp(/(?:\?v=)([^&]+)(?:\&)*/);
      if($scope.recent_blogs[i].video_url!=""){
        $scope.recent_blogs[i].video_thumbnail="";
        var url = $scope.recent_blogs[i].video_url;
        var matches = regex.exec(url);
        var videoId = matches[1];
        $scope.recent_blogs[i].video_thumbnail=videoId;
        console.log($scope.recent_blogs[i].video_thumbnail);
      }      
      i+=1;
    }    
  });

  BlogFactory.popularBlogs().then(function(){
    $scope.popular_blogs = BlogFactory.model.popular_blogs
    console.log($scope.popular_blogs)
    var i=0;
    while(i<$scope.popular_blogs.length){
      var regex = new RegExp(/(?:\?v=)([^&]+)(?:\&)*/);
      if($scope.popular_blogs[i].video_url!=""){
        $scope.popular_blogs[i].video_thumbnail="";
        var url = $scope.popular_blogs[i].video_url;
        var matches = regex.exec(url);
        var videoId = matches[1];
        $scope.popular_blogs[i].video_thumbnail=videoId;
        console.log($scope.popular_blogs[i].video_thumbnail);
      }      
      i+=1;
    }
  });

  if($routeParams.blog_id !== undefined){
    post_id = $routeParams.blog_id
    Postfactory.getPost(post_id).then(function(){
      $scope.blog = Postfactory.model.currentPost;
      console.log($scope.blog);
    })
    Postfactory.getPostComments(post_id).then(function(){
      $scope.comments = Postfactory.model.comments
      console.log($scope.comments);
    })
  }
  $scope.sociallinks=[];
  var sociallinks = SocialFactory.socialLinks().then(function(){
    $scope.sociallinks=SocialFactory.model.links[0].meta.links
    console.log(SocialFactory.model.links[0].meta.links);
  });
  $scope.popitup=function(url) {
    var left = screen.width;
    var top = (screen.height/2)-(225);
    console.log(left);
    console.log(top);
    return window.open(url,"facebook",'height=300,width=500, top=' + top + ', left=' + left);  
  }
  
}])