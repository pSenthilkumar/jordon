app.controller('videoController', ['$rootScope','$routeParams', '$scope','$location','$route','$http', 'Postfactory','VideoFactory','SocialFactory','$timeout', function($rootScope, $routeParams, $scope, $location, $route, $http, Postfactory,VideoFactory,SocialFactory,$timeout) {
  $rootScope.currentPath = $route.current.templateUrl.split("/").pop(-1);
  $scope.featured_videos = [];

  $scope.popitup=function(url) {
    var left = screen.width;
    var top = (screen.height/2)-(225);
    console.log(left);
    console.log(top);
    return window.open(url,"facebook",'height=300,width=500, top=' + top + ', left=' + left);  
  }

  var featured_videos = VideoFactory.getFeaturedVideos().then(function(){
    $scope.featured_videos = VideoFactory.model.featured_videos[0].meta.videos
    if($scope.featured_videos.length>0){
      $scope.current_video = $scope.featured_videos[0].video_url;
    }
    var i=0;
    while(i<$scope.featured_videos.length){
      var regex = new RegExp(/(?:\?v=)([^&]+)(?:\&)*/);
      if($scope.featured_videos[i].video_url!=""){
        $scope.featured_videos[i].video_thumbnail="";
        var url = $scope.featured_videos[i].video_url;
        var matches = regex.exec(url);
        var videoId = matches[1];
        $scope.featured_videos[i].video_thumbnail=videoId;
        console.log($scope.featured_videos[i].video_thumbnail);
      }      
      i+=1;
    }
  }).then(function(){
    if($routeParams.video_id!==undefined){
      var video_id=parseInt($routeParams.video_id);
      console.log(video_id);  
      $scope.current_video = $scope.featured_videos[video_id].video_url;
      console.log($scope.featured_videos[video_id].video_url);
    }    
  })

  $scope.currentSliderClick = 3;

  $scope.slideLeft = function(){
    if ($scope.currentSliderClick != 3) {
      var item_width = jQuery('.home-videoList li').outerWidth() + 10;
      var left_indent = parseInt(jQuery('.home-videoList').css('left')) + item_width;  
      jQuery('.home-videoList').animate({'left' : left_indent},{queue:false, duration:500})
      $scope.currentSliderClick -= 1
    }
  }

  $scope.slideRight = function(){
    var to = ($scope.popular_videos.length > 5)? 6: $scope.popular_videos.length
    if ($scope.currentSliderClick != to) {
      var item_width = jQuery('.home-videoList li').outerWidth() + 10;
      var left_indent = parseInt(jQuery('.home-videoList').css('left')) - item_width;  
      jQuery('.home-videoList').animate({'left' : left_indent},{queue:false, duration:500})
      $scope.currentSliderClick += 1
    }
  }

  var popular_videos = VideoFactory.getPopularVideos().then(function(){
    $scope.popular_videos = VideoFactory.model.popular_videos[0].meta.videos
    var i=0;
    while(i<$scope.popular_videos.length){
      var regex = new RegExp(/(?:\?v=)([^&]+)(?:\&)*/);
      if($scope.popular_videos[i].video_url!=""){
        $scope.popular_videos[i].video_thumbnail="";
        var url = $scope.popular_videos[i].video_url;
        var matches = regex.exec(url);
        var videoId = matches[1];
        $scope.popular_videos[i].video_thumbnail=videoId;
      }      
      i+=1;
    }
  })
  $scope.update_url= function (video){
    // $scope.current_video = video
    videoLink="https://www.youtube.com/embed/"+video+"?start=0&enablejsapi=1"
    jQuery(".videoplayer iframe").attr({"src":videoLink})
    return false;
  }
  $scope.sociallinks=[];
  var sociallinks = SocialFactory.socialLinks().then(function(){
    $scope.sociallinks=SocialFactory.model.links[0].meta.links
    console.log(SocialFactory.model.links[0].meta.links);
  });        
}])