//Main controller
app.controller('musicController', ['$rootScope','$routeParams','$q', '$scope','$location','$route','$http', 'Postfactory', 'MusicFactory','$compile','Comment','UserFactory','Rate','$timeout','SocialFactory', function($rootScope, $routeParams, $q, $scope, $location, $route, $http, Postfactory, MusicFactory,$compile,Comment,UserFactory,Rate,$timeout,SocialFactory) {
  $rootScope.currentPath = $route.current.templateUrl.split("/").pop(-1);
  $scope.contactform = {}
  $scope.rating = 5;
  $scope.rateNoRepeat=0;
  $scope.rateRepeatTime=60;
  $scope.ratingMenu=0;
  $scope.user_id = undefined;
  $scope.userId=undefined;
  $scope.rateUser=0  
  $scope.sociallinks=[];
  $scope.currentPage=-1;
  $scope.pagedResult=[]
  var sociallinks = SocialFactory.socialLinks().then(function(){
    $scope.sociallinks=SocialFactory.model.links[0].meta.links
    console.log(SocialFactory.model.links[0].meta.links);
  });
  $scope.popitup=function(url) {
    var left = screen.width;
    var top = (screen.height/2)-(225);
    console.log(left);
    console.log(top);
    return window.open(url,"facebook",'height=300,width=500, top=' + top + ', left=' + left);  
  }
  $scope.checkUserLogin = function (){
    var deferred = $q.defer();    
    if($scope.user_id==undefined){      
      Postfactory.getUserStatus().then(function(){
        $scope.user_status = Postfactory.model.user_status
        if ($scope.user_status[0]==0) {      
          url=myLocalized.partials+"form.html";
          $http.get(url).then(function(response) {                        
            var template = angular.element(response.data);
            var compiledTemplate = $compile(template);
            compiledTemplate($scope);
            jQuery.fancybox.open({ content: template, type: 'html' });                        
          });
        }
        else{          
          $scope.user_id =$scope.user_status.data.ID
          $scope.rateRepeatTime=2;
        }
        deferred.resolve()        
      });
    }
    else
    {
      deferred.resolve();
    }    
    return deferred.promise;    
  }

  $scope.saveComment = function($event){
    var login_status = $scope.checkUserLogin()
    login_status.then(function(){
      console.log($scope.user_id);
      if($scope.comment.message!='' && $scope.comment.message!=undefined){
        if($scope.user_id!=undefined){
          console.log($scope.comment.message)
          comment_params = {"user_id":$scope.user_id,"comment":$scope.comment.message,"post_id":$scope.playlists[$scope.albumIndex].ID}
          Comment.postComment(null,comment_params);
          $scope.comment={}
          jQuery($event.target).html("Comment Saved");
          $scope.changeComment();
        }
      }
    }).then(function(){
      $scope.commentfunction();
    })
  }
  $scope.perPage=8;



  $scope.pagination=function(page_type){
    if(page_type=="Previous")
    { 
      console.log($scope.currentPage);
      $scope.currentPage--
      if($scope.currentPage>=0)
      {
        starting=($scope.currentPage) * $scope.perPage;
        $scope.pagedResult=[]
        for(i=0;i<$scope.perPage;i++)
        {
          if($scope.playlists[starting]!=undefined)
          {
            $scope.pagedResult.push($scope.playlists[starting])
            starting++
          }
        }
      }
      console.log($scope.currentPage);
    }
    if(page_type=="Next")
    {
      $scope.currentPage++
      if($scope.currentPage<$scope.totalPages)
      {
        starting = ($scope.currentPage) * $scope.perPage;
        $scope.pagedResult=[]
        for(i=0;i<$scope.perPage;i++)
        {
          if($scope.playlists[starting]!=undefined)
          {
            $scope.pagedResult.push($scope.playlists[starting])
            starting++
          }
        }
        console.log($scope.pagedResult);
      }
    }    
    MusicFactory.getRating().then(function(){
      $scope.rateResult = MusicFactory.rateResult
      var k=0;
      while(k<$scope.pagedResult.length){
        var i=0; 
        var j=1;        
        var rateStar=0;
        console.log(k);
        var parsedIndex=parseInt($scope.pagedResult[k].ID);
        $scope.pagedResult[k].rate="";
        var rate_count=0;
        $scope.ratesIcon="";
        while(i < $scope.rateResult.length){
          if (parsedIndex==parseInt($scope.rateResult[i].post)) {
            rateStar=rateStar+parseInt($scope.rateResult[i].rate);
            rate_count+=1;
          }
          i+=1;            
        }
        $scope.rateStarFive=rateStar/rate_count
        console.log($scope.rateStarFive)
        while(j<=$scope.rateStarFive){
          $scope.ratesIcon=$scope.ratesIcon+'<i class="fa fa-star Music-Rate"></i>'
          j+=1;
        }
        $scope.pagedResult[k].rate=$scope.ratesIcon
        k+=1; 
      }            
      console.log($scope.rateIcon);
    }).then(function(){
      var emptybox=8-$scope.pagedResult.length;
      $scope.emptyBox=''
      while(emptybox>0){
        $scope.emptyBox=$scope.emptyBox+"<div class='col-lg-3 col-md-3 col-sm-6 col-xs-6 music-tiles musicsLayout'></div>";
        emptybox-=1;        
      }  
    })
    
  }
  app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
      }
    });

  $scope.contact = function($event){
    console.log("dsafdsafdsafdsa");
    // comment_params = {"First_Name":$scope.firstname,"Last_Name":$scope.lastname,"Email":$scope.email,"Address":$scope.address,"Message":$scope.message}
    error_message = undefined;
    if(is_empty($scope.contactform.firstname)){
      error_message = "Name is missing"
    }
    else if(is_empty($scope.contactform.email)){
      error_message = "Email is missing"
    }
    else if(is_empty($scope.contactform.message)){
      error_message = "Message is required"
    }
    if(error_message!=undefined)
    {
      jQuery($event.target).html(error_message);         
      $scope.toggle();
    }
    else
    { 
      UserFactory.send_mail($scope.contactform)
      $scope.contactform = {}
      jQuery($event.target).html("Message sent successfully");
      $scope.toggle();       
    }
    

  }


  $scope.registerUser=function(){
    if($scope.register.password == $scope.register.password_confirm){
      user_params = {"user_name":$scope.register.name,"user_email":$scope.register.email,"password":$scope.register.password}
      UserFactory.createUser(user_params).then(function(data){
        $scope.user_id=data.user_id
        $scope.saveComment();
        jQuery.fancybox.close();
      }).then(function(){
        $scope.commentfunction();
      })
    }
  }
  $scope.loginUser=function(){
    user_params = {"user_email":$scope.user.email,"password":$scope.user.password}
    if($scope.ForgotPassword==false){
      UserFactory.loginUser(user_params).then(function(data){
        $scope.user_id=data.user_id
        $scope.rateRepeatTime=1;
        $scope.saveComment();
        if ($scope.user_id==0) {
          $scope.logStatus=false
        }
        else {
          $scope.logStatus=true
          jQuery.fancybox.close();
        }
      }).then(function(){
        $scope.commentfunction();
      })
    }
    else{
      UserFactory.forgotPassword(user_params).then(function(data){
        
      })
    }
  }


  if($rootScope.currentPath == 'music.html' || $rootScope.currentPath == 'freemixtape.html') {

    $scope.post = Postfactory.getPosts().then(function(){
      $scope.playlists = Postfactory.model.playlists
      $scope.totalPages=Math.ceil($scope.playlists.length/$scope.perPage);

      $scope.pagination("Next");

      $scope.streamcount = 0;

      for (var i = $scope.playlists.length - 1; i >= 0; i--) {

        var streamcount = 0;
        var playlist = $scope.playlists[i]

        for (var j = playlist.meta.songs.length - 1; j >= 0; j--) {
          streamcount += parseInt(playlist.meta.songs[j].streamcount)
        }

        if($scope.playlists[i] != undefined){
          $scope.playlists[i].streamcount = streamcount;
        }
      };

      if($rootScope.currentPath == 'freemixtape.html') {
        $scope.Views = function(){
          MusicFactory.addListen($scope.playlists[$scope.albumIndex].ID);
        }
        $scope.downloadZip = function(){
          window.open($scope.currentPlaylist.meta.zipfile, 'name'); 
          MusicFactory.addDownload($scope.playlists[$scope.albumIndex].ID);
        }

        $scope.download = function(song){
          window.open(song.src, 'name'); 
          MusicFactory.addDownload($scope.playlists[$scope.albumIndex].ID);
        }
        $scope.albumIndex = 0;
        if($routeParams.album !== undefined){
          for (var i = $scope.playlists.length - 1; i >= 0; i--) {
            if($scope.playlists[i].ID == parseInt($routeParams.album)){
              $scope.albumIndex = i;
              break;
            }
          }
        }

        $scope.embedValue = $scope.playlists[$scope.albumIndex].meta.embed_code
        $scope.pageUrl = $location.absUrl();

        MusicFactory.setPlaying($scope.playlists[$scope.albumIndex]);
        $scope.setPlaylist();
        $scope.commentfunction = function(){
          MusicFactory.getComment($scope.playlists[$scope.albumIndex].ID);
        }
        $scope.commentfunction();
        console.log($scope.music.comment);        
        $scope.rateFetcher=function(){
          MusicFactory.getRating().then(function(){
            $scope.rateResult = MusicFactory.rateResult
            var i=0; 
            var j=1;
            var rateStar=0;
            var parsedIndex=parseInt($scope.playlists[$scope.albumIndex].ID);
            var rate_count=0;
            $scope.rateIcon="";
            while(i < $scope.rateResult.length){
              if (parsedIndex==parseInt($scope.rateResult[i].post)) {
                rateStar=rateStar+parseInt($scope.rateResult[i].rate);
                rate_count+=1;
              }
              i+=1;            
            }
            $scope.rateStarFive=rateStar/rate_count
            console.log($scope.rateStarFive)
            while(j<=$scope.rateStarFive){
              $scope.rateIcon=$scope.rateIcon+'<i class="fa fa-star"></i>'
              j+=1;
            }
            console.log($scope.rateIcon);
          });
        }
        $scope.rateFetcher();                
        $scope.ratingUserRepeat=function(){
          MusicFactory.getRating().then(function() {          
            $scope.rateResult = MusicFactory.rateResult
            var deferred = $q.defer();
            Postfactory.getUserStatus().then(function(){
              $scope.user_status = Postfactory.model.user_status
              if ($scope.user_status[0]!=0) {              
                $scope.user_id = $scope.user_status.data.ID
              }
              deferred.resolve()
            }).then(function(){
              i=0;
              while(i < $scope.rateResult.length){
                if ($scope.rateResult[i].post==$scope.playlists[$scope.albumIndex].ID && $scope.rateResult[i].user== $scope.user_id.toString()) {
                  $scope.rateUser=1;
                  console.log("hi");
                  $scope.rateNoRepeat=1
                  $scope.rateRepeatTime=1;
                  $scope.ratingMenu=2;
                }
                i+=1;            
              }
            })
          })
        }
        $scope.ratingUserRepeat();
        $scope.ratingCheck=function() {
          console.log("1111");
          var login_status=$scope.checkUserLogin();
          login_status.then(function(){
            console.log($scope.rateRepeatTime);
            $scope.ratingUserRepeat();
          }).then(function(){

            if($scope.ratingMenu==0 && $scope.user_id!=undefined && $scope.rateUser==0 && $scope.rateNoRepeat==0) {              
              $scope.ratingMenu=1;              
            }
          })         
        }
        $scope.rateFunction = function(rating) {
          rate_params = {"rate":rating,"post_id":$scope.playlists[$scope.albumIndex].ID,"user_id":$scope.user_id}
          Rate.postRate(null,rate_params);
          var ratingsMenu = function(){
            $scope.ratingMenu=3;
            $scope.rateFetcher();
          };
          $timeout(ratingsMenu, 500);
        };

      }

      // if($rootScope.nowPlaying === undefined){
      //   $scope.startPlaying($scope.playlists[0],$scope.albumIndex);
      // }
    })
}

MusicFactory.topSongs().$promise.then(function(data){
  MusicFactory.setTopSongs(data);
})

$scope.$watch('$scope.user_id',function(){
  console.log($scope.user_id);
})  

$scope.startPlaying = function(playlist){
  MusicFactory.addListen(playlist)
  MusicFactory.setPlaying(playlist);
  $location.path("/freemixtape/"+playlist.ID);
}

$scope.music = MusicFactory.model


$scope.setPlaylist = function(){
  $scope.currentPlaylist = MusicFactory.model.currentPlaylist
  $rootScope.nowPlaying = MusicFactory.model.nowPlaying
  if($scope.currentPlaylist !== undefined){
    $scope.image = $scope.currentPlaylist.meta.front_side_image.url
    $scope.streamCount = 0;
    for (var i = $scope.currentPlaylist.meta.songs.length - 1; i >= 0; i--) {
      $scope.streamCount += parseInt($scope.currentPlaylist.meta.songs[i].streamcount)
    };
  }
}

$scope.playPause = function($index,songTitle){
  if($rootScope.currentTitle == songTitle)
    $rootScope.currentTitle= undefined;
  else
    $rootScope.currentTitle = songTitle

  MusicFactory.addStream($scope.music.currentPlaylist.ID,$index)
  $scope.mediaPlayer.playPause($index)
}

$scope.checkLogin=function(argument) {
  $http({
    method: 'POST',
    url: api_url,
    params: {
      log : $scope.user.name,
      password : $scope.user.password,
      testcookie : 1,
      "wp-submit" : "Log+In"
    }
  }).
  success(function(data, status, headers, config) {
    console.log("success");
  }).
  error(function(data, status, headers, config) {
    console.log("fail");
  });
}
$scope.embedBox = function(){

  var content = jQuery("#embedBox").html();

  jQuery.fancybox.open([
  {
    type: "inline",
    title : "Embed Box",
    content: content,
    autoSize: false
  }
  ], {
    padding : 0,
    width: 500,
    height: 300
  });
}

  // $scope.setPlaylist();
}]).directive("starrating", function() {
  return {
    restrict : "A",
    template : "<ul class='ratingAS'>" +
    "  <li class='rate-selected' ng-repeat='star in stars' ng-class='star' ng-click='toggle($index)'>" +
               "    <i class='fa fa-star'></i>" + //&#9733
               "  </li>" +
               "</ul>",
               scope : {
                ratingValue : "=",
                max : "=",
                onRatingSelected : "&"
              },
              link : function(scope, elem, attrs) {
                var updateStars = function() {
                  scope.stars = [];
                  for ( var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                      filled : i < scope.ratingValue
                    });
                  }
                };
                scope.toggle = function(index) {
                  scope.ratingValue = index + 1;
                  scope.onRatingSelected({
                    rating : index + 1
                  });
                };
                scope.$watch("ratingValue", function(oldVal, newVal) {
                  if (newVal) { updateStars(); }
                });
              }
            };
          });
function is_empty(string_val){
  return (string_val=="" || string_val==undefined);
}